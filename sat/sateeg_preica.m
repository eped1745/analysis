%% SAT-EEG Resting Data
% Specify Data Files for Import
% http://www.bastienboutonnet.com/wiki/doku.php/eeglabsessh1b
% TMS-EEG https://nigelrogasch.gitbooks.io/tesa-user-manual/example_pipelines.html
% https://sccn.ucsd.edu/wiki/Makoto%27s_useful_EEGLAB_code
% https://sccn.ucsd.edu/wiki/Makoto's_preprocessing_pipeline
% MARA plugin https://irenne.github.io/artifacts/
% Cleanline Plugin
% Q/A on Continuous Data https://www.researchgate.net/post/How_to_do_the_preprocessing_of_EEG_data_in_EEGLAb
% PREP data pipeline https://www.frontiersin.org/articles/10.3389/fninf.2015.00016/full

% Overview
% 1. Import data
% Re-sample data
% High pass filter (0.1 to 1 Hz)
% Import channel info (automatic for MFF)
% Remove bad channels
% Interpolate all the removed channels
% Re-reference data to average or linked ears/mastoids
% Remove line noise (may use plug in)
% Epoch data from -1 to 2 seconds
% reject bad epochs for cleaning
% Adjust rank data for ICA
% Run ICA
% Estimate single equivalent current dipoles


%% Set Local Options including turn off Single to Double
EEG.etc.eeglabvers          = '14.1.1'; % this tracks which version of EEGLAB is being used, you may ignore it
pop_editoptions( 'option_storedisk', 0, 'option_savetwofiles', 1, 'option_saveversion6', 1, 'option_single', 0, ...
    'option_memmapdata', 0, 'option_eegobject', 0, 'option_computeica', 1, 'option_scaleicarms', 1, 'option_rememberfolder', 1, ...
    'option_donotusetoolboxes', 0, 'option_checkversion', 1, 'option_chat', 0);

%% Local Variables
eeglab_path     = 'C:\Users\ped8zl\repos\eeglab';
oldpath = path;
path(oldpath,eeglab_path);

datasets_path   = 'E:\datasets\SAT\mff\';
<<<<<<< HEAD
%filename        = 'SAT_rest_pre.226_JA_07.13.2017_20170713_111844.mff\';
%filename        = 'SAT_rest_SAT.104_JE_20160526_105413.mff'; crazy line noise
filename        = 'SAT_rest_SAT.224.post_JS_20170629_031824.mff';
=======
filename        = 'SAT_rest_pre.226_JA_07.13.2017_20170713_111844.mff\';

%datasets_path   = 'C:\Users\ernes\datasets\SH\';
%filename        = 'SAT_rest_pre.226_JA_07.13.2017_20170713_111844.mff\';

>>>>>>> cc87a03cad5a45be5ff73216b6f8213d14620351

%start_time      = 1930;
%end_time        = 2532;
datafile        = strcat(datasets_path,filename);

%% Load datafile into EEG dataset
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
EEG             = pop_readegimff(datafile);
EEG             = eeg_checkset( EEG );
EEG.setname     ='Orig';
eeglab redraw;

%% Delete and Interpolate channel
% Keep original EEG.
%originalEEG = EEG;

% select channels for deletion
%EEG = pop_select( EEG,'nochannel',{'E29'});
%EEG = eeg_checkset( EEG );

% Interpolate channels.
%EEG = pop_interp(EEG, originalEEG.chanlocs, 'spherical');
%EEG = eeg_checkset( EEG );

%% Basic Pre-processing

% resample sampling rate
srate           = 250;      % target sample rate

EEG             = pop_resample( EEG, srate); 
EEG             = eeg_checkset( EEG );

% filtering
lopasshz        = 0.5;  % target low pass
hipasshz        = 100;   % target hi pass

EEG             = pop_eegfiltnew(EEG, [],lopasshz,1650,1,[],0); % low-pass filter 0.5 Hz
EEG             = pop_eegfiltnew(EEG, [],hipasshz,276,0,[],0);

eeglab redraw;
%% Cleanline 60 hz noise removal
EEG             = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:21] ,'computepower',1,'linefreqs',[60 120] ...
                    ,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels', ...
                    'tau',100,'verb',1,'winsize',4,'winstep',1);
EEG.setname     ='Orig-250hz-1hzHi-cleanline';   
EEG             = eeg_checkset( EEG );

%% Re-reference
reref           = 1;        % 1, average 2, mastoids (128)
switch reref
    case 1
        EEG             = pop_reref( EEG, []);          % average re-reference 
    case 2
        EEG             = pop_reref( EEG, [57 100] );    % mastoid reference
end   
EEG                     = eeg_checkset( EEG );
eeglab redraw;


%% EPOCH data into 2 second chunks
% if NoChannels^2 * 30 epoch before ICA
% if insufficient continous then ICA
EEG                         =eeg_regepochs(EEG,'recurrence',2,'limits',[-2 0]);
%EEG                         = pop_rmbase( EEG, [-2000    0]);

EEG                         =eeg_checkset(EEG);
eeglab redraw;

%% Perform EPOCH rejection
EEG = pop_eegthresh(EEG,1,[1:32] ,-300,300,-1,1.996,0,0);

EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
EEG = eeg_checkset( EEG );

%%
% [spectopo_outputs, freqs]= pop_spectopo(EEG, 1, [], 'EEG' , 'winsize' , 512, ...
% 'plot', 'on', 'freqrange',[0 100],'electrodes','on', 'overlap', 0);
% 
% [spectra,freqs] = spectopo(EEG, 0, 1000);
% % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
% deltaIdx = find(freqs>0.5 & freqs<4);
% thetaIdx = find(freqs>4 & freqs<8);
% alphaIdx = find(freqs>8 & freqs<13);
% betaIdx = find(freqs>13 & freqs<40);
% gammaIdx = find(freqs>40 & freqs<80);
% % compute absolute power
% deltaPower = 10^(mean(spectra(deltaIdx))/10);
% thetaPower = 10^(mean(spectra(thetaIdx))/10);
% alphaPower = 10^(mean(spectra(alphaIdx))/10);