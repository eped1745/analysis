%% OCD-EEG Resting Data
% Specify Data Files for Import
% http://www.bastienboutonnet.com/wiki/doku.php/eeglabsessh1b
% TMS-EEG https://nigelrogasch.gitbooks.io/tesa-user-manual/example_pipelines.html
% https://sccn.ucsd.edu/wiki/Makoto%27s_useful_EEGLAB_code
% https://sccn.ucsd.edu/wiki/Makoto's_preprocessing_pipeline
% MARA plugin https://irenne.github.io/artifacts/
% Cleanline Plugin
% Q/A on Continuous Data https://www.researchgate.net/post/How_to_do_the_preprocessing_of_EEG_data_in_EEGLAb
% PREP data pipeline https://www.frontiersin.org/articles/10.3389/fninf.2015.00016/full

%% Set Local Options including turn off Single to Double
eeglab_install_path     = 'C:\Users\ernes\repos\eeglab';
oldpath = path;
path(oldpath,eeglab_install_path);

pop_editoptions( 'option_storedisk', 0, 'option_savetwofiles', 1, 'option_saveversion6', 1, 'option_single', 0, ...
    'option_memmapdata', 0, 'option_eegobject', 0, 'option_computeica', 1, 'option_scaleicarms', 1, 'option_rememberfolder', 1, ...
    'option_donotusetoolboxes', 0, 'option_checkversion', 1, 'option_chat', 0);

%% Local Variables
base_path       = 'C:\Users\ernes\';
eeglab_path     = strcat(base_path,'repos\eeglab\');
oldpath = path;
path(oldpath,eeglab_path);

datasets_path   = strcat(base_path,'datasets\ocdeeg\');
% net_elp         = 'C:\\Users\\ernes\\repos\\eeglab\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp';
net_elp         = strcat(eeglab_path,'plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc');

filename        = 'BM07.edf';
start_time      = 1930;
end_time        = 2532;
datafile        = strcat(datasets_path,filename);

%% Load datafile into EEG dataset
EEG.etc.eeglabvers          = '14.1.1'; % this tracks which version of EEGLAB is being used, you may ignore it
EEG                         = pop_biosig(datafile, 'blockrange',[start_time end_time] );
%EEG                         = pop_biosig('C:\Users\ernes\datasets\OCDEEG\BM07.edf');

EEG.setname                 ='Orig';
EEG                         = eeg_checkset( EEG );
eeglab redraw;

%% Channel Locations, use MNI coordinate file for BEM dipfit model, + 'Optimize Head Center'.
% Per Makoto's preprocessing pipeline
EEG                         = pop_chanedit(EEG, 'lookup',net_elp,'eval','chans = pop_chancenter( chans, [],[]);');

eeglab redraw;

%% Delete channel
% Various plugins - clean_rawdata, PREP pipeline, Makoto trimOutlier
EEG = clean_rawdata(EEG, 5, [0.25 0.75], 0.8, 4, 5, 0.5);
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG,'nochannel',{'L-EOG' 'R-EOG' 'ECGL' 'ECGR'});

%EEG = pop_select( EEG,'channel',{'C3' 'C4' 'O1' 'O2' 'A1' 'A2' 'Cz' 'F3' 'F4' 'F7' 'F8' 'Fz' 'Fp1' 'Fpz' ...
%     'Fp2' 'P3' 'P4' 'Pz' 'T3' 'T4' 'T5' 'T6'}); % remove 'Fpz' 
%EEG = eeg_checkset( EEG );

%% Basic Pre-processing

% resample sampling rate
%srate           = 250;      % target sample rate

%EEG             = pop_resample( EEG, srate); 
%EEG             = eeg_checkset( EEG );

% filtering
lopasshz        = 0.5;  % target low pass
hipasshz        = 100;   % target hi pass

EEG             = pop_eegfiltnew(EEG, [],lopasshz,1650,1,[],0); % low-pass filter 0.5 Hz
EEG             = pop_eegfiltnew(EEG, [],hipasshz,276,0,[],0);

%% Cleanline 60 hz noise removal
EEG             = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:21] ,'computepower',1,'linefreqs',[60 120] ...
                    ,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels', ...
                    'tau',100,'verb',1,'winsize',4,'winstep',1);
EEG.setname     ='Orig-cleanline';   
EEG             = eeg_checkset( EEG );

%% Re-reference
reref           = 1;        % 1, average 2, mastoids (128)
switch reref
    case 1
        EEG             = pop_reref( EEG, []);          % average re-reference 
    case 2
        EEG             = pop_reref( EEG, [57 100] );    % mastoid reference
end   
EEG                     = eeg_checkset( EEG );
eeglab redraw;


%% EPOCH data into 2 second chunks
EEG                         =eeg_regepochs(EEG,'recurrence',2,'limits',[-2 0]);
EEG                         =eeg_checkset(EEG);
eeglab redraw;


% %% for your epoched data, channel 1
%  [spectra,freqs] = spectopo(EEG.data(2,:,:), 0, EEG.srate);
% 
%   % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
%  deltaIdx = find(freqs>1 & freqs<4);
%  thetaIdx = find(freqs>4 & freqs<8);
%  alphaIdx = find(freqs>8 & freqs<13);
%  betaIdx  = find(freqs>13 & freqs<30);
%  gammaIdx = find(freqs>30 & freqs<80);
% 
%   % compute absolute power
%  deltaPower = 10^(mean(spectra(deltaIdx))/10);
%  thetaPower = 10^(mean(spectra(thetaIdx))/10);
%  alphaPower = 10^(mean(spectra(alphaIdx))/10);
%  betaPower  = 10^(mean(spectra(betaIdx))/10);
%  gammaPower = 10^(mean(spectra(gammaIdx))/10);
% 
%  %%
% f = EEG.data;
% f = f(2, :);
% N = EEG.pnts;
% Fs = EEG.srate;
% NFFT = 2^nextpow2(N);
% NOVERLAP = 0;
% WINDOW = 512;
% [spectra, freqs] = pwelch(f, WINDOW, NOVERLAP, NFFT,Fs);
% thetaIdx   = find(freqs>=4 & freqs<=8);            % theta=4-8
% thetaPower = mean(spectra(thetaIdx));              % mean theta power
% alphaIdx   = find(freqs>=8 & freqs<=12);           % alpha=8-12
% alphaPower = mean(spectra(alphaIdx));              % mean alpha power
% matlabThetaAlphaRatio = thetaPower/alphaPower;
% deltaIdx = find(freqs>1 & freqs<4);
% deltaPower = mean(spectra(deltaIdx));
% gammaIdx = find(freqs>30 & freqs<60);
% gammaPower = mean(spectra(gammaIdx));
% 
% 
% deltaPower
% thetaPower
% alphaPower
% gammaPower
